server "staging.digitalkasten.de", user: "deployer", roles: %w{app db web}
set :deploy_to, '/home/deployer/apps_staging/app_frontend'

# Default branch is :master
set :branch, 'free-account'

set :default_env, {
  'NODE_ENV' => 'staging'
}

describe('Root', function() {
  var username, password, loginButton;

  beforeEach(function() {
    browser.ignoreSynchronization = true;
    browser.get('/#/login');
  });

  it('loads', function() {
    element.all(by.cssContainingText('span.button-inner', 'Passwort vergessen')).then(function(items) {
      items[0].getText().then(result => expect(result.toLowerCase()).toBe('passwort vergessen'));
    });
  })
});


import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler, DeepLinkConfig } from 'ionic-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule, Http } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { TranslateModule, TranslateStaticLoader, TranslateLoader } from 'ng2-translate/ng2-translate';
import { CloudSettings, CloudModule } from '@ionic/cloud-angular';
import { Badge } from 'ionic-native';
import { TokenService } from '../providers/token-service';
import { DocumentService } from '../providers/document-service';
import { FolderService } from '../providers/folder-service';
import { UserService } from '../providers/user-service';
import { RegistrationService } from '../providers/registration-service';
import { HttpClient } from '../providers/http-client';
import { ProcessErrorsService } from '../providers/process-errors-service';
import { DisplayMessagesService } from '../providers/display-messages-service';
import { AnalyticsService } from '../providers/analytics-service';
import { BillingService } from '../providers/billing-service';

import { MyApp } from './app.component';
import { AppMenu } from '../components/app-menu/app-menu';

import { Login } from '../pages/login/login';

import { SignupModule, SignupStep1, SignupStep2, SignupStep3, SignupStep4, SignupStep5 } from '../pages/signup';
import { SignUpModule, FreeAccount, Success, CompleteSignUp, BillingOptions, SignUpSuccess } from '../pages/sign-up';
import { DocumentsModule, Inbox } from '../pages/documents';
import { MyData } from '../pages/my-data/my-data';
import { RequestPassword } from '../pages/request-password/request-password';
import { SetPassword } from '../pages/set-password/set-password';
import { ResetPassword } from '../pages/reset-password/reset-password';

import { CreateFolderModal } from '../components/create-folder-modal/create-folder-modal';
import { EditFolderModal } from '../components/edit-folder-modal/edit-folder-modal';
import { Popover } from '../pages/documents/popover/popover';

import { PipeModule } from '../pipes/pipes.module';
import { OnboardingPage } from "../pages/onboarding/onboarding";

const cloudSettings: CloudSettings = {
  'core': {
    'app_id': '44a8c582'
  },
  'push': {
    'sender_id': '5104595532',
    'pluginConfig': {
      'ios': {
        'badge': true,
        'sound': true
      },
      'android': {
        'iconColor': '#343434'
      }
    }
  }
};

export const deepLinkConfig: DeepLinkConfig = {
  links: [
    { component: Login, name: 'Login', segment: 'login' },
    { component: Login, name: 'Login', segment: 'login/:message' },
    { component: Inbox, name: 'Inbox', segment: 'inbox' },
    { component: MyData, name: 'My Data', segment: 'my-data' },
    { component: SignupStep1, name: 'Signup Step 1', segment: 'signup-step1' },
    { component: SignupStep2, name: 'Signup Step 2', segment: 'signup-step2' },
    { component: SignupStep3, name: 'Signup Step 3', segment: 'signup-step3' },
    { component: SignupStep4, name: 'Signup Step 4', segment: 'signup-step4' },
    { component: SignupStep5, name: 'Signup Step 5', segment: 'signup-step5' },
    { component: RequestPassword, name: 'Forget Password', segment: 'forget-password' },
    { component: SetPassword, name: 'Set New Password', segment: 'set-password' },
    { component: ResetPassword, name: 'Reset Password', segment: 'password_resets/:token' },
    { component: FreeAccount, name: 'Free Account', segment: 'free-account' },
    { component: Success, name: 'Success', segment: 'success' },
    { component: CompleteSignUp, name: 'Complete Sign Up', segment: 'complete-sign-up' },
    { component: BillingOptions, name: 'Billing Options', segment: 'billing-options' },
    { component: SignUpSuccess, name: 'Sign Up Success', segment: 'sign-up-success' }
  ]
};

@NgModule({
  declarations: [
    MyApp,
    AppMenu,
    Login,
    MyData,
    RequestPassword,
    SetPassword,
    ResetPassword,
    Popover,
    CreateFolderModal,
    EditFolderModal,
    OnboardingPage
  ],
  imports: [
    IonicStorageModule.forRoot({
     name: 'digitalkasten',
     driverOrder: ['sqlite', 'websql', 'indexeddb']
   }),
    IonicModule.forRoot(MyApp, {}, deepLinkConfig),
    CloudModule.forRoot(cloudSettings),
    ReactiveFormsModule,
    HttpModule,
    SignupModule,
    SignUpModule,
    DocumentsModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    PipeModule
  ],
  bootstrap: [
    IonicApp
  ],
  entryComponents: [
    MyApp,
    Login,
    MyData,
    RequestPassword,
    SetPassword,
    ResetPassword,
    Popover,
    CreateFolderModal,
    EditFolderModal,
    OnboardingPage
  ],
  providers: [
    TokenService,
    DocumentService,
    FolderService,
    RegistrationService,
    HttpClient,
    UserService,
    ProcessErrorsService,
    DisplayMessagesService,
    AnalyticsService,
    BillingService,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    Badge
  ]
})
export class AppModule {}

export function createTranslateLoader(http: Http) {
    return new TranslateStaticLoader(http, 'assets/translations', '.json');
}

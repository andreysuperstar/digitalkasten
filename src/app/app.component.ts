import { Component, ViewChild } from '@angular/core';
import { App, Platform, AlertController, NavController, ModalCmp, ToastCmp, PopoverCmp, AlertCmp, ActionSheetCmp } from 'ionic-angular';
import { StatusBar, Splashscreen, Badge } from 'ionic-native';

import { DocumentService } from '../providers/document-service';
import { UserService } from '../providers/user-service';
import { TranslateService } from 'ng2-translate';

import { Inbox } from '../pages/documents';
import { Login } from '../pages/login/login';
import { CompleteSignUp } from '../pages/sign-up/complete-sign-up/complete-sign-up';
import { MyData } from '../pages/my-data/my-data';

import { TokenService } from '../providers/token-service';
import globals from '../globals';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('nav') nav: NavController;
  rootPage: any;
  public  showMenu: boolean;
  private showCompleteSignupBanner: Boolean = false;
  private showEmailConfirmationBanner: Boolean = false;
  private token: any;
  public language: String = 'en';
  private BANNER_PAGES = [Inbox, MyData, ModalCmp, ToastCmp, PopoverCmp, AlertCmp, ActionSheetCmp];
  public viewsNames: string[] = [
    'Login',
    'FreeAccount',
    'CompleteSignUp',
    'BillingOptions',
    'SignUpSuccess'
  ];
  public currentView: string;

  constructor(
    public app: App,
    public platform: Platform,
    public alertCtrl: AlertController,
    public translate: TranslateService,
    private documentService: DocumentService,
    private userService: UserService,
    private tokenService: TokenService,
  ) {

    // Auto-Detect User-Language
    // var userLang = (<any>navigator).languages && (<any>navigator).languages.length ? (<any>navigator).languages[0].split('-')[0] : (<any>navigator).language.split('-')[0];
    // userLang = /(de|en)/gi.test(userLang) ? userLang : 'en';
    var userLang = globals.locale;
    this.language = userLang;
    // TODO: please make this configurable via a setting in the environment.
    translate.setDefaultLang(userLang);
    translate.use(userLang);

    this.initializeApp();
    this.token = this.tokenService.get();
    if(this.token) {
      this.rootPage = Inbox;
    } else {
      this.rootPage = Login;
    }
  }

  menuOpened() {
    // natural behavior for iOS. Not for other platforms.
    if (this.platform.is('ios')) {
      // StatusBar.hide();
    }
  }

  menuClosed() {
    // natural behavior for iOS. Not for other platforms.
    if (this.platform.is('ios')) {
      // StatusBar.show();
    }
  }

  initializeApp() {
    console.log("app initialized", this);
    this.platform.ready().then(() => {
      StatusBar.styleDefault();
      Splashscreen.hide();
      let inBrowser = (this.platform.is('core') == true || this.platform.is('mobileweb') == true);
      if(!inBrowser){
        // we are in iOS or Android
        Badge.clear();
      }
      
    });
  }

  ngOnInit() {
    console.log("app opened", this);
    this.tokenService
      .subscribe(
        token => {
          if(token) {
            this.showMenu = true;
            this.documentService.all("test");
            this.userService.load()
          } else {
            this.showMenu = false;
            this.documentService.reset();
          }
        },
        e => console.log(e)
      )
    this.app
      .viewWillEnter
      .subscribe(
        view => {
          this.currentView = view.name;

          let match = this.viewsNames
            .some(element => element == view.name);
          if(match) {
            this.showMenu = false;
          } else if(view.name == 'Inbox'){
           this.showMenu = true;
          }
          let allowedPageForBanner = this.BANNER_PAGES.indexOf(view.component) != -1;
          console.log("current view", this.currentView);
          if(allowedPageForBanner){
             // check if we should show the complete signup banner;
            this.userService.getCurrentUser().subscribe(
              user => {
                console.log("fetched the user", user);
                this.showCompleteSignupBanner =  user.canCompleteSignup;
              }, err => {
                console.log("error fetching user", err);
              }
            );
            // check if we should show the pending email confirmation banner
            this.userService.getCurrentUser().subscribe(
              user => {
                console.log("fetched the user", user);
                this.showEmailConfirmationBanner = user.pendingEmailConfirmation;
              }, err => {
                console.log("error fetching user", err);
              }
            );
          } else {
            this.showCompleteSignupBanner = false;
            this.showEmailConfirmationBanner = false;
          }
        },
        error => console.log(error)
      );
  }


  public onCompleteSignUp() {
    this.nav.setRoot(CompleteSignUp);
  }
  
}

import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from 'ng2-translate';

import { User } from '../../models/user';
import { BillingStatement } from '../../models/billing-statement';

import { UserService } from '../../providers/user-service';
import { ProcessErrorsService } from '../../providers/process-errors-service';
import { DisplayMessagesService } from '../../providers/display-messages-service';
import { BillingService } from '../../providers/billing-service';

import { Login } from '../login/login';

@Component({
  selector: 'page-my-data',
  templateUrl: 'my-data.html'
})
export class MyData {
  form: FormGroup;
  user: User = new User();
  statement: BillingStatement;
  loadingData: boolean = true;
  total: number = null;
  errors: any = [];

  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private userService: UserService,
    public formBuilder: FormBuilder,
    private translateService: TranslateService,
    private processErrorsService: ProcessErrorsService,
    private displayMessagesService: DisplayMessagesService,
    private billingService: BillingService
  ) {
    this.initForm();
    this.initBilling();
  }

  ionViewDidEnter() {
    if(!localStorage.getItem('token')) {
      this.navCtrl.setRoot(Login);
    } else {
      this.userService.getCurrentUser().subscribe(
        user => {
          this.user = user;
        },
        err => {
          console.log(this.translateService.instant('myData.loggedOut'));
        });
    }
  }

  initForm() {
    this.form = this.formBuilder.group({
      mailDeliveryFirstName: [
        {
          value: '',
          disabled: true
        },
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(16),
          Validators.pattern(/[a-zA-Z]+/)
        ]
      ],
      mailDeliveryLastName: [
        {
          value: '',
          disabled: true
        },
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(16),
          Validators.pattern(/[a-zA-Z]+/)
        ]
      ],
      mailDeliveryAddress: [
        {
          value: '',
          disabled: true
        },
        Validators.required
      ],
      mailDeliveryCompany: [{
        value: '',
        disabled: true
      }],
      mailDeliveryZip: [
        {
          value: '',
          disabled: true
        },
        [
          Validators.required,
          Validators.minLength(5),
          Validators.pattern(/[0-9]+/)
        ]
      ],
      mailDeliveryCity: [
        {
          value: '',
          disabled: true
        },
        [
          Validators.required,
          Validators.minLength(3),
          Validators.pattern(/[a-zA-Z]+/)
        ]
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
        ]
      ],
      emailConfirmation: '',
      password: '',
      passwordConfirmation: '',
      receiveNotificationsViaPush: [
        true,
        Validators.required
      ],
      receiveNotificationsViaEmail: [
        true,
        Validators.required
      ],
      receiveNewsletter: [
        true,
        Validators.required
      ]
    });
  }

  private initBilling() {
    this.billingService
      .getBillingInfo()
      .subscribe(res => {
        this.statement = res;
        this.loadingData = false;
      });
  }

  public updateUser(event) {
    this.errors = [];
    let updateUser: User = this.convertFormToUser();
    this.userService.update(updateUser).subscribe(
      user => {
        this.user = user;
        this.translateService
          .get('myData.accountUpdated')
          .subscribe(value => this.displayMessagesService.displayMessages([value]));
      },
      err => {
        this.processErrorsService.processErrors(err, this.errors);
      });
  }

  private convertFormToUser(): User {
    let user = new User();
    user.email                        = this.form.get('email').value;
    user.emailConfirmation            = this.form.get('emailConfirmation').value;
    user.mailDeliveryStreet           = this.form.get('mailDeliveryAddress').value;
    user.mailDeliveryCity             = this.form.get('mailDeliveryCity').value;
    user.mailDeliveryCompany          = this.form.get('mailDeliveryCompany').value;
    user.mailDeliveryFirstName        = this.form.get('mailDeliveryFirstName').value;
    user.mailDeliveryLastName         = this.form.get('mailDeliveryLastName').value;
    user.mailDeliveryZipcode          = this.form.get('mailDeliveryZip').value;
    user.password                     = this.form.get('password').value;
    user.passwordConfirmation         = this.form.get('passwordConfirmation').value;
    user.receiveNewsletter            = this.form.get('receiveNewsletter').value;
    user.receiveNotificationsViaEmail = this.form.get('receiveNotificationsViaEmail').value;
    user.receiveNotificationsViaPush  = this.form.get('receiveNotificationsViaPush').value;

    return user;
  }

  public logout() {
    this.userService.logout();
    this.navCtrl.setRoot(Login);
  }

  public confirmDelete() {
    let translate = ['myData.confirmAlert.title', 'myData.confirmAlert.message', 'general.understood'];

    this.translateService
      .get(translate)
      .subscribe(
        (translations) => {
          let title = translations['myData.confirmAlert.title'];
          let message = translations['myData.confirmAlert.message'];
          let understood = translations['general.understood'];
          let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [understood]
          });

          alert.present();
        }
      );
  }
}

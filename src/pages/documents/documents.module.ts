import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { Inbox }  from './inbox/inbox';
import { DocumentList } from './document-list/document-list.component';
import { LabelEditor } from './label-editor/label-editor.component';
import { MoveToFolderModal } from './move-to-folder-modal/move-to-folder-modal.component';
import { TranslateModule } from 'ng2-translate/ng2-translate';

@NgModule({
  imports: [
    IonicModule,
    TranslateModule
  ],
  declarations: [
    Inbox,
    DocumentList,
    LabelEditor,
    MoveToFolderModal
  ],
  entryComponents: [
    Inbox,
    LabelEditor,
    MoveToFolderModal
  ]
})
export class DocumentsModule { }

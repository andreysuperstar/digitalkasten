declare var cordova: any;
import { Component, ViewChild } from '@angular/core';
import { NavController, Platform, ActionSheetController, NavParams, ToastController, LoadingController, ModalController, AlertController, Events, Searchbar } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';

import { Document } from '../../../models/document';
import { DocumentService } from '../../../providers/document-service';
import { FolderService } from '../../../providers/folder-service';
import { UserService } from '../../../providers/user-service';
import { ProcessErrorsService } from '../../../providers/process-errors-service';
import { DisplayMessagesService } from '../../../providers/display-messages-service';
import { AnalyticsService } from '../../../providers/analytics-service';

import { EditFolderModal } from '../../../components/edit-folder-modal/edit-folder-modal';
import { OnboardingPage } from '../../onboarding/onboarding';
import { CompleteSignUp } from '../../sign-up/complete-sign-up/complete-sign-up';

import * as _ from 'lodash';
import 'rxjs/add/operator/map'

@Component({
  selector: 'page-inbox',
  templateUrl: 'inbox.html'
})
export class Inbox {
  public isSearchMode: boolean   = false;
  private filter: any;
  private type: string;
  private title: string;
  private customFolder: any      = null;
  private noContentIcon: string  = 'md-mail-open';
  private noContentMessage: string;
  public dataLoad: any           = true;
  public queryString: string     = "";
  public searchQuery: string     = null;
  private documentsSub: any;
  public documents: Document[]   = [];
  public refresher: any          = null;
  public currentUser: any        = null;
  public canSendLetters: boolean = false;

  private searchBarFocused: boolean = false;
  @ViewChild('searchbar') searchbar: Searchbar;


  constructor(
    private documentService: DocumentService,
    private folderService: FolderService,
    public navCtrl: NavController,
    public platform: Platform,
    public actionsheetCtrl: ActionSheetController,
    private translateService: TranslateService,
    private navParams: NavParams,
    private loadingCtrl: LoadingController,
    private modalCtrl: ModalController,
    private userService: UserService,
    private alertCtrl: AlertController,
    private processErrorsService: ProcessErrorsService,
    private displayMessagesService: DisplayMessagesService,
    private analyticsService: AnalyticsService,
    private events: Events,
    private toastCtrl: ToastController
  ) {
    this.dataLoad = true;
    this.platform = platform;
    this.type = navParams.get('type') || 'inbox';

    // user can be redirected to the inbox if his registration was successful.
    if (navParams.get('registrationSuccess')) {
      this.handleRegistrationSuccess();
      return;
    }

    // user can be redirected to the inbox if his registration was successful.
    if (navParams.get('onboardingCompleted')) {
      this.handleOnboardingCompleted();
      return;
    }

    this.translateService.get('general.menu.inbox').subscribe(
      value => {
        this.title = value;
      }
    );
    this.noContentIcon = 'md-mail-open';
    this.translateService.get('documents.inbox.noLetters').subscribe(
      value => {
        this.noContentMessage = value;
      }
    );
    this.filter = function(d) {
      return d.showInInbox();
    }

    var archive;
    let folder;
    if(folder = navParams.get('folder')) {
      // if is custom folder
      this.customFolder = folder;
      this.title = this.customFolder.title;
      this.filter = function(d) {
        let hasFolders = function(doc) {
          return !doc.deletedAt && doc.folders.length;
        }
        let isInFolder = function(doc, selectedFolder) {
          return _.some(doc.folders, id => {
            return id == selectedFolder.id
          });
        }
        return hasFolders(d) && isInFolder(d, this.customFolder);
      }
    } else {
      switch (this.type) {
        case 'marked':
          this.noContentIcon = 'md-star';
          this.title = this.translateService.instant('general.menu.marked');
          this.noContentMessage = this.translateService.instant('documents.inbox.noMarkedLetters');
          this.filter = function(d) {
            return d.favoritedByUser && !d.deletedAt;
          }
          break;
        case 'sendtome':
          this.translateService.get('general.menu.sendtome').subscribe(
            value => {
              this.title = value;
            }
          );
          this.noContentIcon = 'md-mail-open';
          this.translateService.get('documents.inbox.noNotSendLetters').subscribe(
            value => {
              this.noContentMessage = value;
            }
          );
          this.filter = function(d) {
            return d.pendingUserDelivery && !d.sentToUserAt && !d.deletedAt;
          }
          break;
        case 'trash':
          this.translateService.get('general.menu.trash').subscribe(
            value => {
              this.title = value;
            }
          );
          this.noContentIcon = 'md-trash';
          this.translateService.get('documents.inbox.noDestroyed').subscribe(
            value => {
              this.noContentMessage = value;
            }
          );
          this.filter = function(d) {
            return d.deletedAt;
          }
          break;
        case String(this.type.match(/\w+\-\d{4}/)):
          archive = this.documentService.archive[this.type];
          if(archive) {
            this.title = archive.name;
            this.filter = archive.filter;
          }
          break;
        case 'earlier':
          archive = this.documentService.archive['earlier'];
          if(archive) {
            this.title = archive.name;
            this.filter = archive.filter;
          }
          break;
      }
    }

    this.userService.getCurrentUser().subscribe(
      user => {
        this.canSendLetters = user.registrationState == "complete";
        this.currentUser    = user;
      }
    );
    return this;
  }

  ionViewDidEnter() {
    this.loadDocuments();
  }

  private loadDocuments() {
    let start = performance.now();
    this.documentsSub = this.documentService.documents
      .map(documents => {
        return documents.filter(d => this.filter(d));
      })
      .subscribe(
        documents => {
          this.documents = documents;
          this.hideRefresher();
        },
        err => {
          console.log(err);
          this.hideRefresher(err);
        }
      )
    let end = performance.now();
    console.log("filtering took " + (end - start) + " milliseconds.")
  }

  getItems(ev: any) {
    this.isSearchMode = true;
    this.documentsSub.unsubscribe();
    this.documents = [];
    let val = ev.target.value;
    this.searchQuery = val;
    this.documentService.search(val)
      .map(documents => {
        return documents;
      })
      .subscribe(
        documents => {
          this.documents = documents
        },
        err => {
          console.log(err)
        }
      )
  }

  clear(ev:any) {
    this.isSearchMode = false;
    this.documents = [];
    this.loadDocuments();
  }

  sendLetters($event) {
    if(this.canSendLetters){
      let confirm = this.alertCtrl.create({
        title: this.translateService.instant('general.menu.sendAlert.title'),
        message: this.translateService.instant('general.menu.sendAlert.message'),
        buttons: [
          {
            text: this.translateService.instant('general.menu.sendAlert.doNotSent'),
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: this.translateService.instant('general.menu.sendAlert.send'),
            handler: () => {
              this.userService.requestMailDelivery()
                .subscribe(
                  res => {
                    let confirmMessage = this.translateService.instant('general.menu.confirmAlert.message')
                    this.displayMessagesService.displayMessages([confirmMessage])
                    _.each(this.documents, d => {d.pendingUserDelivery = false});
                    this.documents = []
                  },
                  err => {
                    this.processErrorsService.processErrors(err);
                  }
                );
            }
          }
        ]
      });
      confirm.present();
    } else {
      if (this.currentUser.canCompleteSignup) {
        // redirect to signup
        this.navCtrl.setRoot(CompleteSignUp, {message: "signUp.completeFirst"});
      } else {
        this.toast(this.translateService.instant('general.menu.sendAlert.pleaseConfirmEmailFirst'))
      }
    }
  }

  editFolder(folder) {
    let editFolderModal = this.modalCtrl.create(EditFolderModal, { folder: folder });
    editFolderModal.onDidDismiss(folder => {
      if(folder){
        this.customFolder = folder;
      this.title = folder.title;
      }
      
    });
    editFolderModal.present();
  }

  doRefresh(refresher) {

    if(!this.searchBarFocused){
      this.documentService.all();
      this.refresher = refresher;
    }else{
      //dont allow pull to refresh, so close the refresher
      refresher.cancel();
    }

  }

  hideRefresher(error?) {
    if (this.refresher) {
      if (!error){
        this.refresher.complete();
      }
      else{
        this.refresher.cancel();
      }
      this.refresher = null;
    }
  }

  ngOnInit() {
    // obtain the user to do tracking for the user.
    this.userService.getUserSession()
      .subscribe(
        user => {
          let data = {
            title: this.type, //'Dashboard | digitalkasten.de',
            url: '/inbox',
            CUSTOMER_ID: user.id
          }
          this.currentUser = user;
          // show tutorial dialog here
          if (this.navParams.get("registrationSuccess")) {
            // in this case do not show the tutorial, since we just opened the
            // page.
          } else if (!this.navParams.get("registrationSuccess") && user.showGettingStartedTutorial && !this.userService.tutorialShown) {
            let onboardingPage = this.modalCtrl.create(OnboardingPage);
            onboardingPage.present();
            // mark so we don't show it twice
            this.userService.tutorialShown = true
          }
          this.analyticsService.trackPageView(data);
        },
        err => {
          console.log('failed to load user');
        });
      
    return true;
  }


  ngAfterViewInit(){
    this.searchbar.ionFocus.subscribe(()=>{
          console.log('focused');
          this.searchBarFocused = true;
          this.hideRefresher();

      });
      this.searchbar.ionBlur.subscribe(()=>{
        console.log('blured');
        this.searchBarFocused = false;
      });
  }

  public deleteFolderTapped($event,folder){
    $event.stopPropagation();
    this.events.publish('folder:delete',$event, folder);
  }

  private presentLoadingWithTranslation(translationKey) {
    let spinner = this.platform.is('ios') ? 'ios' : 'crescent';
    let content;
    this.translateService.get(translationKey)
      .subscribe(translation => content = translation);

    let loader = this.loadingCtrl.create({
      spinner: spinner,
      content: content
    });
    loader.present();
  }

  private handleRegistrationSuccess() {
    this.presentLoadingWithTranslation('inbox.waitWhileSettingUp');
    setTimeout(() => {
      // not sure how to do this better, but make sure the browser has a new
      // URL set so that we land on the root inbox page for a logged in user.
      window.location.href = "/";
      window.location.reload();
    },  2000);
    return true;
  }

  private handleOnboardingCompleted() {
    this.presentLoadingWithTranslation('inbox.waitWhileSettingUp');
    setTimeout(() => {
      // not sure how to do this better, but make sure the browser has a new
      // URL set so that we land on the root inbox page for a logged in user.
      window.location.href = "/";
      window.location.reload();
    },  2000);
    return true;
  }

  private toast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
  
}

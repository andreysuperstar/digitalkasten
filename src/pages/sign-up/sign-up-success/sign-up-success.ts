import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Login } from '../../login/login';

@Component({
  selector: 'page-sign-up-success',
  templateUrl: 'sign-up-success.html'
})
export class SignUpSuccess {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  onLogin() {
    this.navCtrl.setRoot(Login);
  }
}

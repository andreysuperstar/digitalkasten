import { Component } from '@angular/core';
import { Platform, NavController, NavParams, ModalController, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TranslateService } from 'ng2-translate';

import { AnalyticsService } from '../../../providers/analytics-service';
import { SignUpService } from '../../../providers/sign-up-service';

import { Terms } from '../terms/terms';
import { Policy } from '../policy/policy';
import { Success } from '../success/success';

@Component({
  selector: 'page-free-account',
  templateUrl: 'free-account.html'
})
export class FreeAccount {
  form: FormGroup;
  terms: boolean = false;
  policy: boolean = false;
  loader: any;
  errors: string[]= [];
  serverErrors: string[] = [];

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public translateService: TranslateService,
    private analyticsService: AnalyticsService,
    private signUpService: SignUpService
  ) {
    this.initForm();
  }

  ionViewDidLoad() {
    let data = {
      'title':'Free Account',
      'url':'free-account'
    }
    this.analyticsService.trackPageView(data);
  }

  initForm() {
    let self = this;
    this.form = this.formBuilder.group({
      emails: this.formBuilder.group(
        {
          email: [
            '',
            [
              Validators.required,
              Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
            ]
          ],
          emailConfirmation: [
            null,
            Validators.required
          ]
        },
        { validator: self.matching('email', 'emailConfirmation') }
      ),
      passwords: this.formBuilder.group(
        {
          password:
          [
            null,
            [
              Validators.required
            ]
          ],
          passwordConfirmation: [
            null,
            [
              Validators.required
            ]
          ]
        },
        { validator: self.matching('password', 'passwordConfirmation') }
      ),
      legal: [
        null,
        Validators.required
      ]
    });
  }

  onSubmit(event: FormGroup) {
    this.errors = [];

    if(this.form.valid) {
      if(this.form.value.legal) {
        this.presentLoading();
        
        this.signUpService
          .signUpFreeAccount(this.form.value)
          .subscribe(
            user => {
              this.loader.dismiss();
              // pass in the user to the success page so that we can then
              // store his token.
              this.navCtrl.setRoot(Success, {
                user: user
              });
              this.trackRegistrationData(user);
            },
            error => {
              console.log(error);
              if(error.isArray)
                this.serverErrors = <string[]>error;
              else {
                this.serverErrors.length = null;
                this.serverErrors.push(<string>error);
              }
              this.loader.dismiss();
            }
          );
      } else {
        this.translateService
          .get('signUp.freeAccount.needToAccept')
          .subscribe(value => this.presentToast(value));
      }
    } else {

      let emailValid = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/).test(this.form.value.emails.email);

      if (!emailValid) {
        this.errors.push(this.translateService.instant('signUp.freeAccount.errors.emailInvalid'));
      }

      if (!this.terms) {
        this.errors.push(this.translateService.instant('signUp.freeAccount.errors.needToAccept'));
      }

      if (!this.form.value.passwords.password || !this.form.value.passwords.passwordConfirmation) {
        this.errors.push(this.translateService.instant('signUp.freeAccount.errors.pickPassword'));
      }

      if (this.form.value.passwords.password != this.form.value.passwords.passwordConfirmation) {
        this.errors.push(this.translateService.instant('signUp.freeAccount.errors.passwordsDontMatch'));
      }

      if (this.form.value.emails.email != this.form.value.emails.emailConfirmation) {
        this.errors.push(this.translateService.instant('signUp.freeAccount.errors.emailsDontMatch'));
      }

      if (!this.form.value.emails.email || !this.form.value.emails.emailConfirmation) {
        this.errors.push(this.translateService.instant('signUp.freeAccount.errors.pickEmail'));
      }

      let message = '';
      let length = this.errors.length;
      for (let i = 1; i <= length; i++) {
        if (i == length) {
          message += this.errors[i-1]
        } else {
          message += this.errors[i-1] + '\n';
        }
      }

      this.presentToast(message);
      console.log("invalid form submitted", this.form);
      return true;
    }
  }

  matching(key: string, confirmationKey: string) {
    return (group: FormGroup) => {
      let input = group.controls[key];
      let confirmationInput = group.controls[confirmationKey];
      if (input.value !== confirmationInput.value) {
        return confirmationInput.setErrors({ notEquivalent: true });
      } else {
        return confirmationInput.setErrors(null);
      }
    }
  }

  readTermsAndConditions(event) {
    event.preventDefault();
    let termsModal = this.modalCtrl.create(Terms);
    if(this.form.controls['legal'].untouched)
      this.form.patchValue({ legal: null });
    termsModal.present();
  }

  readPrivacyPolicy(event) {
    event.preventDefault();
    let policyModal = this.modalCtrl.create(Policy);
    if(this.form.controls['legal'].untouched)
      this.form.patchValue({ legal: null });
    policyModal.present();
  }

  private presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000
    });
    toast.present();
  }

  private presentLoading() {
    let spinner = this.platform.is('ios') ? 'ios' : 'crescent';
    let content;
    this.translateService.get('signUp.loading')
      .subscribe(translation => content = translation);

    this.loader = this.loadingCtrl.create({
      spinner: spinner,
      content: content
    });
    this.loader.present();
  }

  private trackRegistrationData(user) {
    let data = {
      title: 'User created free account',
      url: '/reg/free_account_created',
      orderid: user.id,
    }
    this.analyticsService.trackPageView(data);
  }

  public goBack(event) {
    event.preventDefault();
    this.navCtrl.pop();
  }
}

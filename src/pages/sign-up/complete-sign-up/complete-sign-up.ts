import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TranslateService } from 'ng2-translate';

import * as _ from 'lodash';

import { AnalyticsService } from '../../../providers/analytics-service';
import { SignUpService } from '../../../providers/sign-up-service';
import { RegistrationService } from '../../../providers/registration-service';

import { Inbox } from '../../documents/inbox/inbox';
import { BillingOptions } from '../billing-options/billing-options';

@Component({
  selector: 'page-complete-sign-up',
  templateUrl: 'complete-sign-up.html'
})
export class CompleteSignUp {
  public form: FormGroup;
  public errors: string[] = [];
  public pushPage: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    private translateService: TranslateService,
    private signUpService: SignUpService,
    private registrationService: RegistrationService,
    private analyticsService: AnalyticsService,
    private toastCtrl: ToastController
  ) {
    this.initForm();
    let message = this.navParams.get("message");
    if(message){
      let toast = this.toastCtrl.create({
        message: this.translateService.instant(message),
        duration: 3000,
        position: 'bottom'
      });
      toast.present();
    }

  }

  ionViewDidLoad() {
    let data = {
      'title':'Complete Sign Up',
      'url':'complete-sign-up'
    }
    this.analyticsService.trackPageView(data);
  }

  // sets up the form for the user. inits data already stored for the
  // registration if provided by the user.
  initForm() {
    let self = this;
    this.form = this.formBuilder.group({
      firstName: [
        self.fetchValue('firstName'),
        Validators.required
      ],
      lastName: [
        self.fetchValue('lastName'),
        Validators.required
      ],
      address: [
        self.fetchValue('address'),
        Validators.required
      ],
      company: [self.fetchValue('company')],
      zip: [
        this.fetchValue('zip'),
        Validators.required
      ],
      city: [
        self.fetchValue('city'),
        Validators.required
      ],
      mailDeliveryFirstName: [
        self.fetchValue('mailDeliveryFirstName'),
        Validators.required
      ],
      mailDeliveryLastName: [
        self.fetchValue('mailDeliveryLastName'),
        Validators.required
      ],
      mailDeliveryAddress: [
        self.fetchValue('mailDeliveryAddress'),
        Validators.required
      ],
      mailDeliveryCompany: [self.fetchValue('mailDeliveryCompany')],
      mailDeliveryZip: [
        this.fetchValue('mailDeliveryZip'),
        Validators.required
      ],
      mailDeliveryCity: [
        self.fetchValue('mailDeliveryCity'),
        Validators.required
      ],
    });
  }

  // returns a stored value for the form. this is used since the user can go
  // back and forward between the steps.
  private fetchValue(attributeName, attributeNestedIn = null) {
    return this.signUpService.fetchStoredData(attributeName, attributeNestedIn, 'completeSignUp');
  }

  // copies address data to the mail delivery address data. that way the user
  // does not have to enter his address data twice.
  public copyAddress(event) {
    event.preventDefault();
    let attributes = [
      'firstName',
      'lastName',
      'company',
      'address',
      'zip',
      'city'
    ];
    _.each(attributes, (attr) => {
      var value     = this.form.value[attr];
      var attribute = _.upperFirst(attr);
      var target    = `mailDelivery${attribute}`;
      var data      = {}
      data[target]  = value;
      this.form.patchValue(data);
    });
    return false;
  }

  // called when we submit the form.
  onSubmit(event: FormGroup) {
    this.errors = [];
    if (this.form.valid) {
      this.handleFormValid();
    } else {
      this.handleFormInvalid();
    }
  }

  // called when the form is valid
  private handleFormValid() {
    // store the form in the registration service.
    this.registrationService.stepOneData = this.form;
    this.navCtrl.push(BillingOptions);
  }

  // called when the form is invalid. this will perform several clientside
  // validations to catch obvious errors and then show them to the user.
  private handleFormInvalid() {}

  public onBack() {
    this.navCtrl.setRoot(Inbox);
  }
}

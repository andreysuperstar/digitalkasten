import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Inbox } from '../../documents/inbox/inbox';
import { TokenService } from '../../../providers/token-service';

@Component({
  selector: 'page-success',
  templateUrl: 'success.html'
})
export class Success {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private tokenService: TokenService
  ) {}

  onRedirect() {
    let user = this.navParams.get('user');
    this.tokenService.set(user.token);
    this.navCtrl.setRoot(Inbox, {
      registrationSuccess: true
    });
  }
}

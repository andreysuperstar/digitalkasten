import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from 'ng2-translate/ng2-translate';

import { SignUpService } from '../../providers/sign-up-service';

import { PipeModule } from '../../pipes/pipes.module';

import { FreeAccount } from './free-account/free-account';
import { Terms } from './terms/terms';
import { Policy } from './policy/policy';
import { CompleteSignUp } from './complete-sign-up/complete-sign-up';
import { BillingOptions } from './billing-options/billing-options';
import { SignUpSuccess } from './sign-up-success/sign-up-success';
import { Success } from './success/success';

@NgModule({
  imports: [
    IonicModule,
    ReactiveFormsModule,
    TranslateModule,
    PipeModule
  ],
  declarations: [
    FreeAccount,
    Terms,
    Policy,
    Success,
    CompleteSignUp,
    BillingOptions,
    SignUpSuccess
  ],
  entryComponents: [
    FreeAccount,
    Terms,
    Policy,
    Success,
    CompleteSignUp,
    BillingOptions,
    SignUpSuccess
  ],
  providers: [SignUpService]
})
export class SignUpModule {}

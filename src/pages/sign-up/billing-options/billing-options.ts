import { Component } from '@angular/core';
import { Platform, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TranslateService } from 'ng2-translate';

import globals from '../../../globals';

import { AnalyticsService } from '../../../providers/analytics-service';
import { RegistrationService } from '../../../providers/registration-service';

import { Inbox } from '../../documents/inbox/inbox';

import { UserService } from '../../../providers/user-service';

@Component({
  selector: 'page-billing-options',
  templateUrl: 'billing-options.html'
})
export class BillingOptions {
  public methodForm: FormGroup;
  public bankForm: FormGroup;
  public cardForm: FormGroup;
  public loader: any;
  public errors: string[] = [];
  
  constructor(
  private platform: Platform,
  public navCtrl: NavController,
  public navParams: NavParams,
  public formBuilder: FormBuilder,
  public toastCtrl: ToastController,
  public loadingCtrl: LoadingController,
  private translateService: TranslateService,
  private registrationService: RegistrationService,
  private analyticsService: AnalyticsService,
  private userService: UserService
  ) {
    this.initForm();
  }
  
  ionViewDidLoad() {
    let data = {
      'title':'Billing Options',
      'url':'billing-options'
    }
    this.analyticsService.trackPageView(data);
  }
  
  initForm() {
    let self = this;
    
    this.methodForm = this.formBuilder.group({
      method: [
      self.fetchPaymentMethod('paymentMethod'),
      Validators.required
      ]
    });
    
    this.bankForm = this.formBuilder.group({
      accountHolderName: [
      self.fetchValue('accountHolderName'),
      Validators.required
      ],
      accountNumber: [
      self.fetchValue('accountNumber'),
      Validators.required
      ],
      token: self.fetchValue('token')
    });
    
    this.cardForm = this.formBuilder.group({
      creditCardNumber: [
      self.fetchValue('creditCardNumber'),
      Validators.required
      ],
      name: [
      self.fetchValue('holder'),
      Validators.required
      ],
      expiryMonth: [
      self.fetchValue('expiryMonth'),
      Validators.required
      ],
      expiryYear: [
      self.fetchValue('expiryYear'),
      Validators.required
      ],
      cvc: [
      self.fetchValue('cvc'),
      Validators.required
      ],
      token: self.fetchValue('token'),
      method: self.fetchValue('paymentMethod')
    });
  }
  
  private fetchPaymentMethod(key) {
    if (this.registrationService && this.registrationService.stepTwoData
    && this.registrationService.stepTwoData.card
    && this.registrationService.stepTwoData.card.status
    && this.registrationService.stepTwoData.card.status == 'VALID') {
      return 'card';
    } else {
      return 'bank';
    }
  }
  
  // fetches a payment token for the usser
  private getToken() {
    this.setupStripe();
    if (this.methodForm.value.method == 'card') {
      return this.getTokenCreditCard();
    } else {
      return this.getTokenBankAccount();
    }
  }
  
  // setup stripe with the correct key
  private setupStripe() {
    (<any>window).Stripe.setPublishableKey(globals.stripeKey);
    return true;
  }
  
  // returns a token for the user's entered bank account data.
  private getTokenBankAccount() {
    
    let ibanNumber:string = this.bankForm.get('accountNumber').value;
    ibanNumber = ibanNumber.replace(" ","");
    return new Promise<string>((resolve, reject) => {
      (<any>window).Stripe.source.create({
        type: 'sepa_debit',
        sepa_debit: {
          iban: ibanNumber
        },
        currency: 'EUR',
        owner: {
          name: this.bankForm.get('accountHolderName').value,
          address: {
            line1: this.registrationService.stepOneData.value.address,
            city: this.registrationService.stepOneData.value.city,
            postal_code: this.registrationService.stepOneData.value.zip,
            country: 'DE'
          },
        },
      }, (status: number, response: any) => {
        if (status === 200) {
          resolve(response.id);
        } else {
          reject(response.error.message);
        }
      });
    });
  }
  
  // returns a token for the user's entered credit card.
  private getTokenCreditCard() {
    return new Promise<string>((resolve, reject) => {
      (<any>window).Stripe.card.createToken({
        number: this.cardForm.get('creditCardNumber').value,
        exp_month: this.cardForm.get('expiryMonth').value,
        exp_year: this.cardForm.get('expiryYear').value,
        cvc: this.cardForm.get('cvc').value,
      }, (status: number, response: any) => {
        if (status === 200) {
          resolve(response.id);
        } else {
          reject(response.error.message);
        }
      });
    });
  }
  
  // called when the form is being submitted
  public onSubmit(event: FormGroup) {
    this.presentLoading();
    // get the token from stripe that validates our payment method.
    this.getToken()
    .then(
    token => {
      // store the token correctly.
      if (this.methodForm.value.method == "card") {
        this.cardForm.patchValue({ token: token });
      } else {
        this.bankForm.patchValue({ token: token });
      }
      this.cardForm.patchValue({ method: this.methodForm.value.method })
      this.registrationService.stepTwoData.card  = this.cardForm;
      this.registrationService.stepTwoData.debit = this.bankForm;
      // all good, proceed and store the registration.
      this.registrationService.create(this.registrationService.mapData())
      .subscribe(
      r => {
        // send data to our analytics service
        this.trackRegistrationData(r, this.registrationService);
        // todo handle in case there are errors returned
        this.loader.dismiss();
        this.navCtrl.setRoot(Inbox, {
          onboardingCompleted: true
        });
        
        this.presentToast(this.translateService.instant('signUp.signUpSuccess.title'));
        
        console.log(r);
      },
      err => {
        this.loader.dismiss();
        this.errors = err.errors.full_messages;
      }
      );
    },
    err => {
      this.handleStripeError(err);
      this.loader.dismiss();
    }
    )
  }
  
  // show the given message as a toast to the user
  private presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
  
  // show a loading animation to the user.
  private presentLoading() {
    let spinner = this.platform.is('ios') ? 'ios' : 'crescent';
    let content;
    this.translateService.get('signUp.completingRegistration')
    .subscribe(translation => content = translation);
    
    this.loader = this.loadingCtrl.create({
      spinner: spinner,
      content: content
    });
    this.loader.present();
  }
  
  // returns a stored value for the form. this is used since the user can go
  // back and forward between the steps.
  private fetchValue(attributeName, attributeNestedIn = null) {
    return this.registrationService.fetchStoredData(attributeName, attributeNestedIn, 'stepTwoData');
  }
  
  private trackRegistrationData(user, registrationForm) {
    let valueMapping = {
      'flexible_plan': 100,
    }
    let plan = 'flexible_plan';
    let data = {
      title: 'Payment data added',
      url: '/reg/payment_data_added',
      orderid: user.id,
      subscription: plan,
      subscription_value: valueMapping[plan]
    }
    this.analyticsService.trackPageView(data);
  }
  
  private handleStripeError(err) {
    if (err.indexOf('sepa_debit[iban]') >= 0) {
      // Invalid IBAN: "The payment method `sepa_debit` requires the parameter: sepa_debit[iban]."
      this.presentToast(this.translateService.instant('signUp.stripeError.invalidIban'));
    } else if (err.indexOf('owner[name]') >= 0) {
      // Invalid owner: "You passed an empty string for 'owner[name]'. We assume empty values are an attempt to unset a parameter; however 'owner[name]' cannot be unset. You should remove 'owner[name]' from your request or supply a non-empty value."
      this.presentToast(this.translateService.instant('signUp.stripeError.invalidOwner'));
    } else {
      this.presentToast(err);
    }
    // Invalid IBAN: "The payment method `sepa_debit` requires the parameter: sepa_debit[iban]."
  }
  
  onBack() {
    this.navCtrl.pop();
  }
}


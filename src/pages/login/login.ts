import { Component } from '@angular/core';

import { NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { TranslateService } from 'ng2-translate';

import { Inbox } from '../documents';

import { LoginService } from '../../providers/login-service';
import { TokenService } from '../../providers/token-service';
import { FreeAccount } from '../sign-up/free-account/free-account';
import { RequestPassword } from '../request-password/request-password';
import { UserService } from "../../providers/user-service";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [
    LoginService
  ]
})

export class Login {
  pushPage: any = null;
  forgetPassword: any = null;
  session: any = null;
  errorMessage: boolean = false;

  public loginForm = this.fb.group({
    email: ["", Validators.required],
    password: ["", Validators.required]
  });

  constructor(
    public nav: NavController,
    public fb: FormBuilder,
    public loginService: LoginService,
    public toastCtrl: ToastController,
    private translateService: TranslateService,
    private storage: Storage,
    private tokenService: TokenService,
    private userService: UserService,
    private params: NavParams, 
    private loadingCtrl: LoadingController
  ) {
    this.pushPage = FreeAccount;
    this.forgetPassword = RequestPassword;
  }
  public login = (event) => {
    console.log(event);
    let loadingCtrlOptions = {
      content: this.translateService.instant('login.wait')
    };
    let loader = this.loadingCtrl.create(loadingCtrlOptions);
    loader.present();

    this.loginService.login({email: this.loginForm.value.email, password: this.loginForm.value.password})
      .subscribe(
        session => {
          this.tokenService.set(session['token']);
          // load the user first so we 
          // know the authentication state
          // and can show the banner properly
          this.userService.load().subscribe( user =>{
            this.nav.setRoot(Inbox);
            loader.dismiss();
          }, error =>{
            loader.dismiss();
            console.log("failed to load user");
          });
        },
        err => {
          loader.dismiss();
          if (err.status == 403) {
            let message = "Bitte bestätige Deine Emailadresse";
            let toast = this.toastCtrl.create({
              message: message,
              duration: 3000
            });
            toast.present();
          } else {
            this.translateService.get('login.credentialsDontMatch').subscribe(
              value => {
                let message = value;
                let toast = this.toastCtrl.create({
                  message: message,
                  duration: 3000
                });
                toast.present();
              }
            );
          }
        }
      )
  }

  ionViewDidEnter() {
    if(this.params.get('message') == 'registration_success') {
      this.translateService.get('login.accountActivated').subscribe(
        value => {
          let message = value;
          let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
          });
          toast.present();
        }
      );
    }
  }
}

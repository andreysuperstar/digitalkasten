import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from "rxjs/Observable";

import { BillingStatement } from '../models/billing-statement';

import { TokenService} from  './token-service';

import globals from '../globals';

@Injectable()
export class BillingService  {
    billingStatement: BillingStatement;
    private baseUrl = `${globals.apiUrl}/billing_statements`;

    constructor(
      private http: Http,
      private tokenService: TokenService
    ) { }

    getBillingInfo(): Observable<BillingStatement> {
        return this.http
          .get(this.baseUrl + "?token=" + this.tokenService.get())
          .map(res => res.json())
          .map(res => res.shift());
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from './http-client';
import 'rxjs/add/operator/map';
import globals from '../globals';

@Injectable()
export class LoginService {
  private baseUrl = `${globals.apiUrl}/sessions`;

  constructor(public http: HttpClient) {
    this.http = http;
  }

  login(opts) {
    return this.http.post(this.baseUrl, opts).map(res => res.json())
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '../providers/http-client';
import { Document } from '../models/document';
import { TokenService } from '../providers/token-service';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import {TranslateService} from 'ng2-translate';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import * as _ from 'lodash';
import * as moment from 'moment';
import 'moment/locale/de';
import globals from '../globals';

// inpired by on https://coryrylan.com/blog/angular-observable-data-services
@Injectable()
export class DocumentService {
  public loading: boolean = false;
  public archive: Object = {};
  public counts: Object = {};
  private baseUrl = `${globals.apiUrl}/documents`;
  private dataStore: {
    documents: Document[]
  };
  private _documents: BehaviorSubject<Document[]>;
  public documents: Observable<Document[]>;
  
  constructor (
  private http: HttpClient,
  private tokenService: TokenService,
  private translateService: TranslateService
  ) {
    this.dataStore = { documents: [] };
    this._documents = <BehaviorSubject<Document[]>>new BehaviorSubject([]);
    this.documents = this._documents.asObservable();
    this.archive = this.buildArchive(6);
    this.documents.subscribe(documents => this.counts = this.updateCounts(documents));
  }
  
  public reset() {
    this.dataStore = { documents: [] };
    this._documents.next(Object.assign({}, this.dataStore).documents);
  }
  
  public search(val): Observable<Document[]> {
    this.loading = true;
    return this.http.get(this.baseUrl + '?token=' + this.tokenService.get() + "&q=" + val)
    .map(response => {
      this.loading = false;
      return response.json().map(d => new Document(d));
    })
    .catch((response) => Observable.throw(response.json()))
  }
  
  public all(folderId: string = ""): Observable<Document[]> {
    this.loading = true;
    if (folderId != "") {
      this.http.get(this.baseUrl + '?token=' + this.tokenService.get())
      .map(response => {
        this.loading = false;
        return response.json().map(d => new Document(d));
      })
      .catch((response) => Observable.throw(response.json()))
      .subscribe(
      data => {
        this.dataStore.documents = data;
        this._documents.next(Object.assign({}, this.dataStore).documents);
      }, err => {
        console.log(err);
      });
    } else {
      this.http.get(this.baseUrl + '?token=' + this.tokenService.get())
      .map(response => {
        this.loading = false;
        return response.json().map(d => new Document(d));
      })
      .subscribe(
      data => {
        this.dataStore.documents = data;
        this._documents.next(Object.assign({}, this.dataStore).documents);
      }, err => {
        console.log(err);
      });
    }
    return this.documents;
  }
  
  public update(document: Document): Observable<Document> {
    return Observable.create(observer => {
      this.http.put(this.baseUrl + '/' + document.id + '?token=' + this.tokenService.get(), document.toJson())
      .map((response) => new Document(response.json()))
      .catch((response) => Observable.throw(response.json()))
      .subscribe(
      data => {
        this.dataStore.documents.forEach((d, i) => {
          if (d.id === data.id) { this.dataStore.documents[i] = data; }
        });
        this._documents.next(Object.assign({}, this.dataStore).documents);
        observer.next(data);
        observer.complete();
      },
      err => {
        console.log('Could not update document.')
        observer.error(err);
        observer.complete();
      }
      );
    });
  }
  
  public destroy(document) {
    document.deletedAt = new Date();
    return this.update(document);
  }
  
  // public remove(documentId: string) {
    //   // this.http.delete(this.baseUrl + '/' + document.id + '?token=' + token, document.toJson())...
    //   this.dataStore.documents.forEach((d, i) => {
      //     if (d.id == documentId) {
        //       this.dataStore.documents.splice(i, 1);
        //     }
        //   });
        //   this._documents.next(Object.assign({}, this.dataStore).documents);
        // }
        
        public buildArchive(archiveMonths: number) {
          var t0 = performance.now();
          let months = {};
          var userLang = navigator.language.split('-')[0];
          userLang = /(de|en)/gi.test(userLang) ? userLang : 'en';
          moment.locale(userLang);
          for(let i = 0; i < archiveMonths; i++) {
            let month = moment().add(i * -1, 'months');
            let monthName = moment.months(month.month()) + " " + month.year();
            let monthSlug = slugify(monthName);
            months[monthSlug] = {
              month: month.month(),
              name: moment.months(month.month()) + " " + month.year(),
              moment: month,
              filter: function(d) {
                return !d.showInInbox() && !d.deletedAt && month.month() == moment(d.createdAt).month();
              }
            };
          };
          let earlier = moment().add(-1 * archiveMonths, 'months');
          
          months['earlier'] = {
            month: 'earlier',
            name: "Früher",
            moment: earlier,
            filter: function(d) {
              return !d.showInInbox() && !d.deletedAt && moment(d.createdAt).isBefore(earlier);
            }
          }
          var t1 = performance.now();
          console.log("building archive took " + (t1 - t0) + " milliseconds.")
          return months;
        }
        
        public updateCounts(documents) {
          var t0 = performance.now();
          let counts = {
            "all": 0,
            "marked": 0,
            "notSent": 0,
            "destroy": 0,
            "pendingUserDelivery": 0,
            "months": {},
            "folders": {
              "inbox": 0,
              "marked": 0,
              "sendtome": 0,
              "trash": 0
            }
          };
          
          // Setup time based archives
          if(this.archive) {
            for(let archive in this.archive) {
              counts["months"][this.archive[archive].month] = 0;
            }
          }
          
          // Only take documents that are not opened yet.
          documents = _.filter(documents, d => !d.openedByUser);
          
          // Apply counts for each folder.
          for(let document of documents) {
            for(let folderSlug of document.folderSlugs) {
              if(counts.folders[folderSlug]) {
                counts.folders[folderSlug]++;
              } else {
                counts.folders[folderSlug] = 1;
              }
            }
          }
          // Special logic for inboxes.
          counts.folders.inbox = 0;
          for(let document of documents) {
            // if deleted - don't show anywhere else.
            if(document.deletedAt) {
              counts.folders.trash += 1;
            } else {
              // override custom counting for inbox.
              if(document.showInInbox()) {
                counts.folders.inbox += 1;
              }
              if(document.favoritedByUser) {
                counts.folders.marked += 1;
              }
              if(document.pendingUserDelivery) {
                counts.folders.sendtome += 1;
              }
              if (document.showInArchive()) {
                if(moment(document.createdAt).isBefore(this.archive['earlier'].moment)) {
                  counts.months['earlier']++;
                } else {
                  counts.months[moment(document.createdAt).month()]++;
                }
              }
            }
            
          }
          var t1 = performance.now();
          console.log("updating counts took " + (t1 - t0) + " milliseconds.")
          return counts;
        }
        
        markDocumentAsUnread(document: Document):Observable<Document>{
          document.openedByUser = false;
          return this.update(document);
        }
      }
      
      
      
      
      function slugify(text) {
        return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '');            // Trim - from end of text
      }
      
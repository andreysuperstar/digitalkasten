import { Injectable } from '@angular/core';

@Injectable()
export class AnalyticsService  {

  // track a page view by the user. pass options that will be passed to the
  // tracking API.
  public trackPageView(options = {}) {
    setTimeout(() => {
      this.pushDataToGoogle(options);
    }, 2000)
  }

  // proceeds and pushes the data to the google analytics web service
  private pushDataToGoogle(options) {
    console.info('tracking event with data', options);
    this.getDataLayer().push({
      'event':'virtualPageView',
      'page': options
    });
  }

  // returns the data layer service by google.
  private getDataLayer() {
    let fallback = {
      push: function(opts) {
        return opts;
      }
    }
    // see the following code on why we use the window object like this.
    // http://stackoverflow.com/questions/12709074/how-do-you-explicitly-set-a-new-property-on-window-in-typescript
    return window['dataLayer'] || fallback;
  }
}

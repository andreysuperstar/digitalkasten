import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import globals from '../globals';

@Injectable()
export class SignUpService {
  private baseUrl = `${globals.apiUrl}`;
  completeSignUp: any = {};
  billingOptions: any = {
    card: null,
    bank: null,
  };

  constructor(public http: Http) {}

  // fetch stored data from the registration service
  //
  // attributeName: the name of the attribute to search for
  // attributeNestedIn: the subform where the attribute can be found in. used
  // in case form groups are used.
  // registrationStep: the step of the registration to look in.
  //
  // returns the value of the attribute or an empty string.
  fetchStoredData(attributeName, attributeNestedIn, registrationStep): void {
    let step = this[registrationStep];

    if(!step)
      return null;

    if(step[attributeName])
      // check for root level stored values on the registration service.
      return step[attributeName];

    else if(step[attributeNestedIn] && step[attributeNestedIn][attributeName])
      // it could also happen that we are storing a nestefd property in the
      // form. in that case check for the subkey
      return step[attributeNestedIn][attributeName];

    else if(this.shouldPrefillRegistrationWithTestData())
      // for testing/dev prefill some data in the registration
      return this.fetchPrefilledRegistrationTestData(attributeName);

    else
      // nothing found, return an empty string
      return null;
  }

  // returns true if we should prefill the registration with test data.
  private shouldPrefillRegistrationWithTestData(): boolean {
    return globals.prefillRegistration;
  }

  // returns prefilled registration data
  private fetchPrefilledRegistrationTestData(attributeName) {
    let data = {
      firstName: 'John',
      lastName: 'Doe Tester',
      address: 'Foostreet 12',
      company: 'Doe Ltd',
      zip: '21075',
      city: 'Hamburg',
      mailDeliveryFirstName: 'Ally',
      mailDeliveryLastName: 'Doe Tester',
      mailDeliveryAddress: 'Friedelstrasse 12',
      mailDeliveryCompany: 'Mail Ltd',
      mailDeliveryZip: '10245',
      mailDeliveryCity: 'Berlin'
    }
    return data[attributeName];
  }

  mapData() {
    let data = {}
    if(this.completeSignUp) {
      data['first_name']               = this.completeSignUp.firstName;
      data['last_name']                = this.completeSignUp.lastName;
      data['street']                   = this.completeSignUp.address;
      data['company']                  = this.completeSignUp.company;
      data['zipcode']                  = this.completeSignUp.zip;
      data['city']                     = this.completeSignUp.city;
      data['mail_delivery_first_name'] = this.completeSignUp.mailDeliveryFirstName;
      data['mail_delivery_last_name']  = this.completeSignUp.mailDeliveryLastName;
      data['mail_delivery_street']     = this.completeSignUp.mailDeliveryAddress;
      data['mail_delivery_company']    = this.completeSignUp.mailDeliveryCompany;
      data['mail_delivery_zipcode']    = this.completeSignUp.mailDeliveryZip;
      data['mail_delivery_city']       = this.completeSignUp.mailDeliveryCity;
    }

    if(this.billingOptions) {
      if (this.billingOptions.card && this.billingOptions.card.token) {
        data['token'] = this.billingOptions.card.token;
      } else {
        data['token'] = this.billingOptions.bank.token;
      }
    }

    let userLang = navigator.language.split('-')[0];
    userLang = /(de|en)/gi.test(userLang) ? userLang : 'en';
    data['locale'] = userLang;
    // for now always DE
    data['locale'] = 'de';

    data = {
      onboarding: data
    };

    return data;
  }

  signUpFreeAccount(data: any): Observable<any> {
    let url = `${this.baseUrl}/registrations`;
    data = {
      registration: {
        email: data.emails.email,
        email_confirmation: data.emails.emailConfirmation,
        password: data.passwords.password,
        password_confirmation: data.passwords.passwordConfirmation,
        accepted_tos: true,
        accepted_privacy: true
      }
    };
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    let headers = new Headers({ 'Accept': 'application/json; version=2' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, data, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  signUp(): Observable<any> {
    let url = `${this.baseUrl}/onboardings`;
    let data = this.mapData();
    // let headers = new Headers({ 'Content-Type': 'application/json' });
    let headers = new Headers({ 'Accept': 'application/json; version=2' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(url, data, options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();

    return body || { };
  }

  private handleError (error: Response | any) {
    let err: string[] | string | any;
    let errMsg: string;

    if (error instanceof Response) {
      const body = error.json() || 'Unknown error';
      if (body && body.errors) {
        err = body.errors.full_messages || JSON.stringify(body);
      } else {
        err = ['An unknown error occurred. Please try again later.'];
      }
      let errors: string;
      if (err.isArray)
        errors = err
          .join('\n');
      errMsg = `${error.status} - ${error.statusText || ''}\n${errors || err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(err);
  }
}

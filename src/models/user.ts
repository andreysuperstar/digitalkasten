export class User {
  id: number;
  email: String;
  emailConfirmation: String;
  firstName: String;
  lastName: String;
  street: String;
  company: String;
  zipcode: String;
  city: String;
  receiveNotificationsViaPush: Boolean;
  receiveNotificationsViaEmail: Boolean;
  receiveNewsletter: Boolean;
  nextMailDeliveryAt: String;
  deviceTokens: Array<String>;
  password: String;
  passwordConfirmation: String;
  locale: String;
  registrationState: string;
  canCompleteSignup: Boolean;
  pendingEmailConfirmation: Boolean;
  showGettingStartedTutorial: Boolean;

  mailDeliveryFirstName: String;
  mailDeliveryLastName: String;
  mailDeliveryStreet: String;
  mailDeliveryCompany: String;
  mailDeliveryZipcode: String;
  mailDeliveryCity: String;

  token: string;
  method: string;

  constructor(data: any = null) {
    if (data) {
      this.id                           = data.id;
      this.email                        = data.email;
      this.emailConfirmation            = data.email_confirmation;
      this.firstName                    = data.first_name;
      this.lastName                     = data.last_name;
      this.street                       = data.street;
      this.company                      = data.company;
      this.zipcode                      = data.zipcode;
      this.city                         = data.city;
      this.receiveNotificationsViaPush  = data.receive_notifications_via_push;
      this.receiveNotificationsViaEmail = data.receive_notifications_via_email;
      this.receiveNewsletter            = data.receive_newsletter;
      this.nextMailDeliveryAt           = data.next_mail_delivery_at;
      this.deviceTokens                 = data.device_tokens;
      this.password                     = data.password;
      this.passwordConfirmation         = data.passwordConfirmation;
      this.locale                       = data.locale;
      this.registrationState            = data.registration_state;
      this.canCompleteSignup            = data.can_complete_signup;
      this.pendingEmailConfirmation     = data.pending_email_confirmation;
      this.showGettingStartedTutorial   = data.show_getting_started_tutorial;
      if (data.mail_delivery_address) {
        this.mailDeliveryFirstName = data.mail_delivery_address.first_name;
        this.mailDeliveryLastName = data.mail_delivery_address.last_name;
        this.mailDeliveryStreet = data.mail_delivery_address.street;
        this.mailDeliveryCompany = data.mail_delivery_address.company;
        this.mailDeliveryZipcode = data.mail_delivery_address.zipcode;
        this.mailDeliveryCity = data.mail_delivery_address.city;
      }
      if (data.token) {
        this.token = data.token;
        if (data.method)
          this.method = data.method;
      }
    }
  }

  public toJson() {
    return {
      "user": {
        "id": this.id,
        "device_tokens": this.deviceTokens,
        "email": this.email,
        "email_confirmation": this.emailConfirmation,
        "mail_delivery_first_name": this.mailDeliveryFirstName,
        "mail_delivery_last_name": this.mailDeliveryLastName,
        "mail_delivery_street": this.mailDeliveryStreet,
        "mail_delivery_company": this.mailDeliveryCompany,
        "mail_delivery_zipcode": this.mailDeliveryZipcode,
        "mail_delivery_city": this.mailDeliveryCity,
        "receive_notifications_via_push": this.receiveNotificationsViaPush,
        "receive_notifications_via_email": this.receiveNotificationsViaEmail,
        "receive_newsletter": this.receiveNewsletter,
        "password": this.password,
        "password_confirmation": this.passwordConfirmation,
        "locale": this.locale
      }
    }
  }

  shouldUpdatePushNotificationToken() {
    return (!this.deviceTokens.length && !this.receiveNotificationsViaPush) || this.receiveNotificationsViaPush;
  }

}

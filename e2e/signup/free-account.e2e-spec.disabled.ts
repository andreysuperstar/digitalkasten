import { browser, element, by, ElementFinder, protractor } from 'protractor';


function waitForUrlToChangeTo(urlRegex) {
    var currentUrl;

    return browser.getCurrentUrl().then(function storeCurrentUrl(url) {
            currentUrl = url;
        }
    ).then(function waitForUrlToChangeTo() {
            return browser.wait(function waitForUrlToChangeTo() {
                return browser.getCurrentUrl().then(function compareCurrentUrl(url) {
                    return urlRegex.test(url);
                });
            });
        }
    );
}


describe('Free account  signup E2E Test', () => {
    let signup = (email:string,pass:string)=>{
         var emailFields = element.all(by.css("input[type=email]"));
        
        emailFields.each( (el)=>{
            el.sendKeys(email);
        });
        
        var passwordFields = element.all(by.css("input[type=password]"));
        passwordFields.each((el)=>{
            el.sendKeys(pass);
        });
        
        emailFields.each((el) => {
            expect(el.getAttribute("value")).toEqual(email);
        });
        
        passwordFields.each((el) => {
            expect(el.getAttribute("value")).toEqual(pass);
        });
        
        var termsAndConditionsField = element(by.css("ion-checkbox"));
        termsAndConditionsField.click();
        var submitButton = element(by.css("button[type=submit]"));
        submitButton.click();

    };
    beforeEach(() => {
        browser.get("/#/free-account");
        browser.ignoreSynchronization = true;
    });
    it("have the correct path",()=>{
        expect(browser.getCurrentUrl()).toMatch(/\/free-account$/);
    });
    it("the  signup form is displayed by default", ()=>{
        var form = element(by.id("signup-form"));
        expect(form.isPresent()).toBeTruthy();
    });
    it("contains email and password fields", ()=>{
        var emailFields = element.all(by.css("ion-input[type=email]"));
        var passwordFields = element.all(by.css("ion-input[type=password]"));
        
        expect(emailFields.count()).toEqual(2);
        expect(passwordFields.count()).toEqual(2);
        
    });
    
    it("contains a terms and condition field",()=>{
        var termsAndConditionsField = element(by.css("ion-checkbox"));
        expect(termsAndConditionsField.isPresent()).toBeTruthy();
    });
    
    it("can enter new user data ",()=>{
        var emailFields = element.all(by.css("input[type=email]"));
        var email = "test1@test.de";
        var pass = "test1";
        emailFields.each( (el)=>{
            el.sendKeys(email);
        });
        
        var passwordFields = element.all(by.css("input[type=password]"));
        passwordFields.each((el)=>{
            el.sendKeys(pass);
        });
        
        emailFields.each((el) => {
            expect(el.getAttribute("value")).toEqual(email);
        });
        
        passwordFields.each((el) => {
            expect(el.getAttribute("value")).toEqual(pass);
        });
        
    });
    it("contains a submit button" ,()=>{

        var submitButton = element(by.css("button[type=submit]"));
        expect(submitButton.isPresent()).toBeTruthy();
    })
    it("can check the terms and conditions checkbox",()=>{
        var termsAndConditionsField = element(by.css("ion-checkbox"));
        termsAndConditionsField.click();
        expect(termsAndConditionsField.isElementPresent(by.css(".checkbox-icon.checkbox-checked"))).toBeTruthy();
        
    });
    it("show a progress dialog after submiting the submit form",()=>{
        
       signup("test4@test.de","test");
       browser.wait(function() {
             var deferred = protractor.promise.defer();
             var q = element(by.css('ion-loading')).isPresent()
            q.then( function (isPresent) {
                     deferred.fulfill(isPresent);
             });
        return deferred.promise;
    }, 10000);
       
       waitForUrlToChangeTo(/success$/);
       browser.driver.sleep(30000); 
    });
    it("a success page is shown after signup",()=>{
        signup("test3@test.de",'test');
        browser.driver.sleep(5000);
        let successBox =  element(by.id("success-box"));
        expect(successBox.isPresent()).toBeTruthy();
    });

});


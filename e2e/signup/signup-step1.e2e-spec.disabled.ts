import { browser, element, by, ElementFinder, protractor } from 'protractor';
 




describe('Signup  step 2 E2E Test', () => {
 
   var fillPersonalDetails;  
   var fillNewCredentials;
   var fillDeliveryAddress;
  beforeEach(() => {

    browser.ignoreSynchronization = true;
    browser.get('/#/signup-step1');
    
     fillPersonalDetails = ()=>{
      let firstNameField = element(by.css('ion-input[formControlName="firstName"] input'));
      let lastNameField = element(by.css('ion-input[formControlName="lastName"] input'));
      let companyField = element(by.css('ion-input[formControlName="company"] input'));
      let addressField = element(by.css('ion-input[formControlName="address"] input'));
      let zipField = element(by.css('ion-input[formControlName="zip"] input'));
      let cityField = element(by.css('ion-input[formControlName="city"] input'));

      expect(firstNameField.isPresent()).toBeTruthy();
      expect(lastNameField.isPresent()).toBeTruthy();
      expect(companyField.isPresent()).toBeTruthy();
      expect(addressField.isPresent()).toBeTruthy();
      expect(zipField.isPresent()).toBeTruthy();
      expect(cityField.isPresent()).toBeTruthy();
      

      firstNameField.sendKeys("Miguel");
      lastNameField.sendKeys("Carvajal");
      companyField.sendKeys("Tripl");
      addressField.sendKeys("Address");
      zipField.sendKeys("1");
      cityField.sendKeys("City");


    };
    fillNewCredentials=  ()=>{
      let emailField = element(by.css('ion-input[formControlName="email"] input'));
      let emailConfirmField = element(by.css('ion-input[formControlName="emailConfirmation"] input'));
      let passwordField = element(by.css('ion-input[formControlName="password"] input'));
      let passwordConfirmField = element(by.css('ion-input[formControlName="passwordConfirmation"] input'));

      expect(emailField.isPresent()).toBeTruthy();
      expect(emailConfirmField.isPresent()).toBeTruthy();
      expect(passwordField.isPresent()).toBeTruthy();
      expect(passwordConfirmField.isPresent()).toBeTruthy();

      emailField.sendKeys("test1@test.de");
      emailConfirmField.sendKeys("test1@test.de");
      passwordField.sendKeys('test');
      passwordConfirmField.sendKeys('test');
    };

    fillDeliveryAddress = ()=> {
      let copyAddressBtn = element(by.id('copy-address-btn'));
      copyAddressBtn.click();
    };

  });
 
  it('the signup form is present', () => {
      let form = element(by.id('signup-step1-form'));
      expect(form.isPresent()).toBeTruthy();

  });
   
  it('the signup form contains a personal info section ', () => {
      let form = element(by.id('personal-info-section'));
      expect(form.isPresent()).toBeTruthy();
     
  });
  it('the signup form contains a mail info section ', () => {
      let form = element(by.id('mail-info-section'));
      expect(form.isPresent()).toBeTruthy();
     
  });

  it('the signup form contains a mail info section ', () => {
      let form = element(by.id('mail-info-section'));
      expect(form.isPresent()).toBeTruthy();
     
  });

  it('the signup form contains a credentials section ', () => {
      let form = element(by.id('credentials-section'));
      let fields = element.all(by.css("#credentials-section input"))
      expect(form.isPresent()).toBeTruthy();
      expect(fields.count()).toEqual(4);
     
  }); 

  it('the signup form contains a privacy policy section ', () => {
      let section = element(by.id('privacy-policy-section'));
      expect(section.isPresent()).toBeTruthy();

  });
  it('I can enter a new email and password', ()=>{

      fillNewCredentials();

     

  });
   it('I can enter my personal info', ()=>{
       fillPersonalDetails();
    
  });
  it('I can duplicate address using a button', ()=>{

      fillPersonalDetails();
      fillDeliveryAddress();

      let firstNameField = element(by.css('ion-input[formControlName="mailDeliveryFirstName"] input'));
      let lastNameField = element(by.css('ion-input[formControlName="mailDeliveryLastName"] input'));
      let companyField = element(by.css('ion-input[formControlName="mailDeliveryCompany"] input'));
      let addressField = element(by.css('ion-input[formControlName="mailDeliveryAddress"] input'));
      let zipField = element(by.css('ion-input[formControlName="mailDeliveryZip"] input'));
      let cityField = element(by.css('ion-input[formControlName="mailDeliveryCity"] input'));

      expect(firstNameField.isPresent()).toBeTruthy();
      expect(lastNameField.isPresent()).toBeTruthy();
      expect(companyField.isPresent()).toBeTruthy();
      expect(addressField.isPresent()).toBeTruthy();
      expect(zipField.isPresent()).toBeTruthy();
      expect(cityField.isPresent()).toBeTruthy();
      
      expect(firstNameField.getAttribute('value')).toMatch("Miguel");
      expect(lastNameField.getAttribute('value')).toMatch("Carvajal");
      expect(companyField.getAttribute('value')).toMatch("Tripl");
      expect(addressField.getAttribute('value')).toMatch("Address");
      expect(zipField.getAttribute('value')).toMatch("1");
      expect(cityField.getAttribute('value')).toMatch("City");
  });


  it("I can complete step 1 of signup", ()=>{

      fillNewCredentials();
      fillPersonalDetails();
      fillDeliveryAddress();
      let termsCheckbox = element(by.css("ion-checkbox[formControlName='checkedTermsAndConditions']"));
      let privacyCheckbox = element(by.css("ion-checkbox[formControlName='checkedPrivacyPolicy']"));

      termsCheckbox.click();
      privacyCheckbox.click();
      
      let registerBtn =  element(by.css("form button[type='submit']"));
      expect(registerBtn.isPresent()).toBeTruthy();

      var EC = protractor.ExpectedConditions;
      browser.wait(EC.elementToBeClickable(registerBtn),5000);
      browser.driver.sleep(1000);
     
  });


});
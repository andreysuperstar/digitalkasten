import { browser, element, by, ElementFinder, protractor } from 'protractor';
 

describe('Signup step 3 E2E Test', () => {
 
   beforeEach(() => {

        browser.get("/#/signup-step3")
   });
   it('contains a payment form',()=>{

       var elem = element(by.css('form.paymentSelect'));
       expect(elem.isPresent()).toBeTruthy();
       
   });
   it('contains a select box for the payment type',()=>{

       var elem = element(by.css('ion-select[formControlName=payment]'));
       expect(elem.isPresent()).toBeTruthy();

   });

});
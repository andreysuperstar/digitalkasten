import {element, browser, by} from 'protractor';
import {login} from '../utils/utils';

describe ("Free account billing options  E2E Test", ()=>{
    
    beforeAll(() => {
        
        
        browser.get('#/login');
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
        browser.ignoreSynchronization = true;
        login();
        browser.driver.sleep(3000);
    });
    beforeEach(() => {
        browser.get('#/billing-options');
        browser.driver.sleep(1000);
    });
    it('contains a method form',()=>{
        let form = element(by.id('method-form'));
        expect(form.isDisplayed()).toBeTruthy();
    });
    
    it('can select a billing option',()=>{

        let selectElement = element(by.id('method-select'));
        expect(selectElement.isDisplayed()).toBeTruthy();
        selectElement.click();
        browser.driver.sleep(3000);
    });
});
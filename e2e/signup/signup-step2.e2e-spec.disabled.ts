import { browser, element, by, ElementFinder, protractor } from 'protractor';





describe('Signup step 2 E2E Test', () => {


    beforeEach(() => {
        browser.get('/#/signup-step2');
        browser.ignoreSynchronization = true;
    });
    it("plan items are shown by default",()=>{

        let planItems = element.all(by.css(".plan-item"));
        expect(planItems.count()).toEqual(3);
    });
    it("I can select 12 months plan",()=>{
        let button  = element(by.css('#twelve-months-plan button'));
        expect(button.isPresent()).toBeTruthy();
        button.click().then(()=>{
            browser.driver.sleep(1000);
            expect(browser.getCurrentUrl()).toMatch(/step3$/);
        });
    });
    it("I can select 6 months plan",()=>{
        let button  = element(by.css('#six-months-plan button'));
        expect(button.isPresent()).toBeTruthy();
        button.click().then(()=>{
            browser.driver.sleep(1000);
            expect(browser.getCurrentUrl()).toMatch(/step3$/);
        });
    });
    it("I can select flexible plan",()=>{
        let button  = element(by.css('#flexible-plan button'));
        expect(button.isPresent()).toBeTruthy();
        button.click().then(()=>{
            browser.driver.sleep(1000);
            expect(browser.getCurrentUrl()).toMatch(/step3$/);
        });
    });


});
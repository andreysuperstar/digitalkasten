import { browser, element, by, protractor } from 'protractor';


describe('Free account  complete signup E2E Test', () => {
    
    
    let firstNameValue = 'Miguel';
    let lastNameValue = 'Carvajal';
    let companyValue = 'Digitalkasten';
    let addressValue = 'Ave Bustillo';
    let zipValue  = '8400';
    let cityValue = 'Bariloche';
    
    let login = function(){
        
        let emailField = element(by.css('ion-input[type="email"] input'));
        let passwordField = element(by.css('ion-input[type="password"] input'));
        
        let email = 'test_email_confirmed@test.de';
        let password = 'test';
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        let loginBtn = element(by.css(".submit-btn"));  
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.elementToBeClickable(loginBtn),5000);
        loginBtn.click();
        
        
    }
    let testEnterAddress = function(id:string, fill:boolean = true){
        let {firstName, lastName, company, address, zip, city } = getFormFields(id);
        
        if(fill){
            firstName.clear();
            lastName.clear();
            company.clear();
            address.clear();
            zip.clear();
            city.clear();
            
            firstName.sendKeys(firstNameValue);
            lastName.sendKeys(lastNameValue);
            company.sendKeys(companyValue);
            address.sendKeys(addressValue);
            zip.sendKeys(zipValue);
            city.sendKeys(cityValue);            
        }
        
        expect(firstName.getAttribute("value")).toMatch(firstNameValue);
        expect(lastName.getAttribute("value")).toMatch(lastNameValue);
        expect(company.getAttribute("value")).toMatch(companyValue);
        expect(address.getAttribute("value")).toMatch(addressValue);
        expect(zip.getAttribute("value")).toMatch(zipValue);
        expect(city.getAttribute("value")).toMatch(cityValue);
        
    }
    
    
    let getFormFields = function(id){
        
        if(id ===  'second-address'){
            var firstName =  element(by.css('#' + id + " ion-input[formControlName=mailDeliveryFirstName] input"));
            var lastName =  element(by.css('#' + id + " ion-input[formControlName=mailDeliveryLastName] input"));
            var company =  element(by.css('#' + id + " ion-input[formControlName=mailDeliveryCompany] input"));
            var address =  element(by.css('#' + id + " ion-input[formControlName=mailDeliveryAddress] input"));
            var zip =  element(by.css('#' + id + " ion-input[formControlName=mailDeliveryZip] input"));
            var city =  element(by.css('#' + id + " ion-input[formControlName=mailDeliveryCity] input"));
        }else{
            var firstName =  element(by.css('#' + id + " ion-input[formControlName=firstName] input"));
            var lastName =  element(by.css('#' + id + " ion-input[formControlName=lastName] input"));
            var company =  element(by.css('#' + id + " ion-input[formControlName=company] input"));
            var address =  element(by.css('#' + id + " ion-input[formControlName=address] input"));
            var zip =  element(by.css('#' + id + " ion-input[formControlName=zip] input"));
            var city =  element(by.css('#' + id + " ion-input[formControlName=city] input"));
        }
        
        
        
        expect(element.all(by.css('#' + id + " ion-input")).count()).toBe(6);    
        return {firstName, lastName, company, address, zip, city};
    }
    
    beforeEach(() => {
        browser.get("#/login");
        browser.ignoreSynchronization = true;
        login();
        browser.driver.sleep(15000);
        expect(browser.getCurrentUrl()).toMatch(/inbox$/);
        browser.driver.sleep(7000);
    });
    it("have a banner on the top",() =>{
        var banner = element(by.css(".user-notification-banner"));
        browser.driver.sleep(7000);
        expect(banner.isPresent()).toBeTruthy();
        
        // browser.get("#//complete-sign-up");
        // browser.driver.sleep(7000);
    });
    it("can go to the signup page",() =>{
        var banner = element(by.css(".user-notification-banner"));
        browser.driver.sleep(7000);
        expect(banner.isPresent()).toBeTruthy();
        var link =  element(by.css(".user-notification-banner a"));
        expect(link.isDisplayed()).toBeTruthy();
        link.click().then( ()=>{
            expect(browser.getCurrentUrl()).toMatch(/sign-up$/);
        });
        // browser.get("#//complete-sign-up");
        // browser.driver.sleep(7000);
    });
    it("can get out of the signup page",() =>{
        var banner = element(by.css(".user-notification-banner"));
        browser.driver.sleep(7000);
        expect(banner.isPresent()).toBeTruthy();
        var link =  element(by.css(".user-notification-banner a"));
        expect(link.isDisplayed()).toBeTruthy();
        link.click().then( ()=>{
            browser.driver.sleep(1000);
            expect(browser.getCurrentUrl()).toMatch(/sign-up$/);
        });
        let backBtn  = element(by.id('backBtn'));
        backBtn.click().then( ()=>{
            browser.driver.sleep(1000);
            expect(browser.getCurrentUrl()).toMatch(/inbox$/);
        });
    });
    
    it("a address form is shown by default",() =>{
        
        browser.get("#/complete-sign-up");
        let form = element(by.css("form"));
        expect(form.isDisplayed()).toBeTruthy();
        let lists =  element.all(by.css("form ion-list"));
        expect(lists.count()).toBe(2);
        getFormFields('first-address');
        getFormFields('second-address');
        
    });
    
    it("can type address info",()=>{
        browser.get("#/complete-sign-up");
        testEnterAddress('first-address');
        
    });
    it("can type second address info",()=>{
        browser.get("#/complete-sign-up");
        
        testEnterAddress('second-address');
        
        
    });
    it("can copy one adress to other", () => {
        browser.get("#/complete-sign-up");
        testEnterAddress('first-address');
        let copyBtn = element(by.id("copy-btn"));
        copyBtn.click();
        // now the second address field should contain the same values
        testEnterAddress('second-address',false);
        
    });
    it("can complete step",() => {

        browser.get("#/complete-sign-up");

        testEnterAddress('first-address');
        testEnterAddress('second-address');
        let continueBtn =  element(by.id('continue-btn'));
        expect(continueBtn.isDisplayed()).toBeTruthy();
        continueBtn.click().then(() => {
            // check for loading indicator and page change
            browser.driver.sleep(1000);
            expect(browser.getCurrentUrl()).toMatch(/billing-options$/);
        });
    });
    
    afterEach(function() {
        browser.executeScript('window.sessionStorage.clear();');
        browser.executeScript('window.localStorage.clear();');
    });
});
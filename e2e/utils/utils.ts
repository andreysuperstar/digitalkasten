import {element,by, protractor, browser} from 'protractor';

 export let login = function(){
        
        let emailField = element(by.css('ion-input[type="email"] input'));
        let passwordField = element(by.css('ion-input[type="password"] input'));
        
        let email = 'test_email_confirmed@test.de';
        let password = 'test';
        emailField.sendKeys(email);
        passwordField.sendKeys(password);
        let loginBtn = element(by.css(".submit-btn"));  
        var EC = protractor.ExpectedConditions;
        browser.wait(EC.elementToBeClickable(loginBtn),5000);
        loginBtn.click();
        
        
}

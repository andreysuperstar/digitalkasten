import { browser, element, by, ElementFinder,protractor } from 'protractor';
 
describe('Login E2E Test', () => {
 
  beforeEach(() => {

    browser.get('/#/login');
    browser.ignoreSynchronization = true;
    
      
    
  });
  it("we are in the correct path", ()=>{

      expect(browser.getCurrentUrl()).toMatch(/login$/);

  }); 

  it('the login form is displayed by default', () => {

     
      let elem = element(by.css('.login-box'));
      expect(elem.isPresent()).toBeTruthy();
     
  });
 it('be able to login with credentials', () => {
      

       var urlChanged = function(url) {
        return function () {
          return browser.getCurrentUrl().then(function(actualUrl) {
            return url != actualUrl;
          });
        };
      };
      let emailField = element(by.css('ion-input[type="email"] input'));
      let passwordField = element(by.css('ion-input[type="password"] input'));
      expect(emailField.isPresent()).toBeTruthy();
      expect(passwordField.isPresent()).toBeTruthy();
      let email = 'test@test.de';
      let password = 'test';
      emailField.sendKeys(email);
      passwordField.sendKeys(password);
      let loginForm =  element(by.css('.login-box form'));
      expect(loginForm.isPresent()).toBeTruthy();
      
      let loginBtn = element(by.css(".submit-btn"));  
      expect(loginBtn.isPresent()).toBeTruthy();

      var EC = protractor.ExpectedConditions;
      browser.wait(EC.elementToBeClickable(loginBtn),5000);
      loginBtn.click().then( () =>{

          browser.driver.sleep(5000);
          // expect(browser.getCurrentUrl()).toMatch(/inbox$/);  
      });
      
      
       
  });
 
  
 
});